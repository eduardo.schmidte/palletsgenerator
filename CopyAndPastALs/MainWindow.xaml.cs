﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CopyAndPastALs
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
       

        public MainWindow()
        {
            InitializeComponent();
            Keyboard.Focus(class_n);
            FocusManager.SetFocusedElement(this, class_n);

            paletas_totais.Text = Directory.GetFiles(@"input\", "*.pal", SearchOption.TopDirectoryOnly).Count().ToString();
           
        }

       
        private void BTN_start_Click(object sender, RoutedEventArgs e)
        {

            List<string> list_names_masc = new List<string>() { class_n.Text };
            List<string> list_names_fem = new List<string>() { class_n.Text };

            string sourceDir = @"input\";
            string backupDir = @"output\";

            string saida_fem = @"¿©\";
            string saida_masc = @"³²\";

            string[] pal_files = Directory.GetFiles(sourceDir, "*.pal");

            #region criardirectory
            //cria caso nao tem o diretorio output
            if (!Directory.Exists(backupDir))
            {
                Directory.CreateDirectory(backupDir);
            }

            //cria caso nao tem o diretorio output\¿©\
            if (!Directory.Exists(backupDir + saida_fem))
            {
                Directory.CreateDirectory(backupDir + saida_fem);
            }

            //cria caso nao tem o diretorio output\³²\
            if (!Directory.Exists(backupDir + saida_masc))
            {
                Directory.CreateDirectory(backupDir + saida_masc);
            }
            #endregion

            int count = 0;
            int count2 = 0;

            foreach (string pal_names in pal_files)
            {
                string fileName = pal_names.Substring(sourceDir.Length);
                // string newNames = list_names_masc.ToString();
                // string newNames_masc = list_names_fem.ToString();


                foreach (string names in list_names_fem)
                {
                    String pad = ("_" + count).PadLeft(1, '0');

                    //fem
                    File.Copy(System.IO.Path.Combine(sourceDir, fileName),
                        System.IO.Path.Combine(backupDir + saida_fem, names + "_¿©" + pad + ".pal"), true);
                    count++;
                    
                }

                foreach (string names2 in list_names_masc)
                {
                    String pad2 = ("_" + count2).PadLeft(1, '0');

                    //masc
                    File.Copy(System.IO.Path.Combine(sourceDir, fileName),
                        System.IO.Path.Combine(backupDir + saida_masc, names2 + "_³²" + pad2 + ".pal"), true);
                   count2++;
                    
                }
            }

            mensagem_principal.Text = "Finalizado!";
        }
    }
}
